/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pornchitar.final_oop_64160260;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class WriteAnimal {
    public static void main(String[] args) {
        ArrayList<Zoo>zoo = new ArrayList();
        zoo.add(new Zoo("Koala bear","Terrestrial Animal","Australia","Imported on 22 February 2013."));
        zoo.add(new Zoo("Jaguar","Terrestrial Animal","United States of America","Imported on 1 September 2014."));
        zoo.add(new Zoo("Caiman","Amphibians","Mexico","Imported on 8 May 2015."));
        zoo.add(new Zoo("Piranha","Aquatic Animal","Brazil","Imported on 17 November 2016."));
        
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try{
            file = new File("zoo.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(zoo);
            oos.close();
            fos.close();
        }catch (FileNotFoundException ex){
            Logger.getLogger(WriteAnimal.class.getName()).log(Level.SEVERE,null,ex);
        }catch (IOException ex){
            Logger.getLogger(WriteAnimal.class.getName()).log(Level.SEVERE,null,ex);
        }
    }
}
