/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pornchitar.final_oop_64160260;

import java.io.Serializable;

/**
 *
 * @author ASUS
 */
public class Zoo implements Serializable{

    private String Name;
    private String Type;
    private String Countries;
    private String Description;

    public Zoo(String Name, String Type, String Countries, String Description) {
        this.Name = Name;
        this.Type = Type;
        this.Countries = Countries;
        this.Description = Description;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getCountries() {
        return Countries;
    }

    public void setCountries(String Countries) {
        this.Countries = Countries;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    @Override
    public String toString() {
        return "Zoo{" + "Name=" + Name + ", Type=" + Type + ", Countries=" + Countries + ", Description=" + Description + '}';
    }
}
