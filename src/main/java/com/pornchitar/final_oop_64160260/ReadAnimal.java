/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pornchitar.final_oop_64160260;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class ReadAnimal {
    public static void main(String[] args) {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try{
            file = new File("zoo.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            ArrayList<Zoo>zoo = (ArrayList<Zoo>)ois.readObject();
            System.out.println(zoo);
            fis.close();
        }catch (FileNotFoundException ex){
            Logger.getLogger(ReadAnimal.class.getName()).log(Level.SEVERE,null,ex);
        }catch (IOException ex){
            Logger.getLogger(ReadAnimal.class.getName()).log(Level.SEVERE,null,ex);
        }catch (ClassNotFoundException ex){
            Logger.getLogger(ReadAnimal.class.getName()).log(Level.SEVERE,null,ex);
        }
    }
}
